<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

$app = new Silex\Application();

require_once 'db-bootstrap.php';

// replace with mechanism to retrieve EntityManager in your app
$entityManager = $app['orm.em'];

$platform = $entityManager->getConnection()->getDatabasePlatform();
$platform->registerDoctrineTypeMapping('enum', 'string');

return ConsoleRunner::createHelperSet($entityManager);
