<?php

namespace Model;

use Doctrine\ORM\Mapping as ORM;
/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity()
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="stringId", type="string", length=10, unique=true)
     */
    private $stringId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set stringId
     *
     * @param string $stringId
     * @return Client
     */
    public function setStringId($stringId)
    {
        $this->stringId = $stringId;

        return $this;
    }

    /**
     * Get stringId
     *
     * @return string
     */
    public function getStringId()
    {
        return $this->stringId;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

}
