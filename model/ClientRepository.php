<?php

namespace Cms\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class ClientRepository
 */
class ClientRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function getListAllForPaginate()
    {
        $q = $this
            ->createQueryBuilder('c')
            ->select('c');

        return $q->getQuery()->getResult();
    }
}
