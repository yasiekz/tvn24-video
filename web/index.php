<?php

use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../env.php';

$app = new Silex\Application();

$app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__ . "/../config/prod.yml"));
if (APPLICATION_ENV != 'prod') {
    $app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__ . "/../config/" . strtolower(APPLICATION_ENV) . ".yml"));
}
$app['debug'] = $app['parameters']['debug'];

$app->register(new Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../logs/'. strtolower(APPLICATION_ENV) .'.log',
));

if ($app['parameters']['memcache']['enabled']) {
    $app->register(
        new SilexMemcache\MemcacheExtension(),
        array(
            'memcache.library' => $app['parameters']['memcache']['library'],
            'memcache.server' => $app['parameters']['memcache']['servers']
        )
    );
}

$app->before(
    function (Request $request, Application $app) {
        if ($request->getMethod() === Request::METHOD_GET && $app['parameters']['memcache']['enabled']) {
            $cached = $app['memcache']->get($request->getPathInfo());
            if ($cached) {
                return new Response($cached);
            }
        }
    }
);

$app->finish(
    function (Request $request, Response $response, Application $app) {

        if ($request->getMethod() === Request::METHOD_GET && $app['parameters']['memcache']['enabled']) {
            $cached = $app['memcache']->get($request->getPathInfo());
            if (!$cached) {
                $app['memcache']->set($request->getPathInfo(), $response->getContent(), $app['parameters']['memcache']['defaultCacheLifetime']);
            }
        }
    }
);

require_once '../db-bootstrap.php';

$app->get(
    '/',
    function () use ($app) {

        /** @var \Cms\UserBundle\Entity\ClientRepository $repo */
        $repo = $app['orm.em']->getRepository('Model\Client');
        $clients = $repo->findAll();

        $serializer = JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($clients, 'json');

        return new Response($jsonContent);
    }
);

$app->run();
