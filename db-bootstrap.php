<?php

use Dflydev\Silex\Provider\DoctrineOrm\DoctrineOrmServiceProvider;

$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'dbs.options' => array (
        'default' => array(
            'driver'    => 'pdo_mysql',
            'host'      => 'localhost',
            'dbname'    => 'deltima-beta',
            'user'      => 'root',
            'password'  => null,
            'charset'   => 'utf8mb4',
        ),
        'rw' => array(
            'driver'    => 'pdo_mysql',
            'host'      => 'localhost',
            'dbname'    => 'deltima-beta',
            'user'      => 'root',
            'password'  => null,
            'charset'   => 'utf8mb4',
        ),
    ),
));

$app->register(new DoctrineOrmServiceProvider, array(
    "orm.proxies_dir" => __DIR__.'/../cache/doctrine/proxies',
    "orm.em.options" => array(
        "mappings" => array(
            array(
                "type" => "annotation",
                "namespace" => "Model",
                "path" => __DIR__.'/model',
                'use_simple_annotation_reader' => false,
            ),
        ),
    ),
));
